package io.github.daschner.example.example;

import com.google.inject.Inject;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.filter.Getter;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.plugin.Dependency;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextStyles;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

@Plugin(
        id = "example",
        name = "Example",
        version = "0.0.1",
        description = "Example plugin for MineYourMind to get the TPS via a command.",
        authors = {
                "TheRedHun1",
                "Daschnerj"
        }
)
public class Example {

    public static long previousTime = 0;
    public static long currentTime = 0;

    public static ArrayList<Double> tps = new ArrayList<>();

    Task.Builder taskBuilder = Task.builder();

    public static String prefix = "[Example]: ";

    @Inject
    private Logger logger;

    @Inject
    public Example(Logger logger) {
        this.logger = logger;

        //
    }

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        getLogger().info(prefix + "Example plugin started.");
        Task task = Task.builder().execute(() ->
        {
            double time = (((Example.previousTime-50.0))/(double)Example.currentTime) * 20.00;
            //getLogger().info("TPS: " + time);
            if(tps.size() == 10)
                tps.remove(0);
            tps.add(time);
            previousTime = currentTime;
            currentTime = System.currentTimeMillis();
        })
                .async().interval(50, TimeUnit.MILLISECONDS)
                .name("Example - TPS Task").submit(this);
        Sponge.getCommandManager().register(this, tpsCommand, "exampletps", "etps");
    }

    @Inject
    private void setLogger(Logger logger) {
        this.logger = logger;
    }

    public Logger getLogger() {
        return logger;
    }

    //Commands
    CommandSpec tpsCommand = CommandSpec.builder()
            .description(Text.of("Gets the TPS of the server."))
            .permission("example.command.tps")
            .executor(new TPSCommand())
            .build();




}
