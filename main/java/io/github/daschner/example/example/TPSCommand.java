package io.github.daschner.example.example;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.Text;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.concurrent.atomic.AtomicReference;

public class TPSCommand implements CommandExecutor {
    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.DOWN);
        src.sendMessage(Text.of(Example.prefix + "The current TPS of the Server is: " + df.format(getAverage()) +"."));
        return CommandResult.success();
    }

    private double getAverage()
    {
        AtomicReference<Double> n = new AtomicReference<>(0.0);
        Example.tps.forEach(x -> n.updateAndGet(v -> new Double((double) (v + x))));
        return n.get()/10.0;
    }
}
